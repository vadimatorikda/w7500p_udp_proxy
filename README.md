# What contains this repository?
This repository contains example proxy-server. It converts UDP packages from microcontroller (uP7 library format) to Baical packages (P7 library format).
For more details see [here](https://habr.com/ru/post/570720/).

# How build it?
You must have CMake.
```
mkdir build && cd build && cmake .. && make
```

# How start it?
You need start Baical before.
```
./build/w7500p_udp_proxy /your_path/w7500p_up7_test/up7/ 5270 6720 10000

```
