#include <iostream>

extern "C" {
#include "uP7.h"
}

#include "submodules/lib_up7_proxy/uP7proxyApi.h"
#include "PTime.h"

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

void die (const char *s) {
    perror(s);
    exit(1);
}

int main (int arg_num, char *args[]) {
    // Check arg number.
    if (arg_num < 4) {
        printf("Usage: path_to_up7_gen_dir port mc_frequency");
        return -1;
    }

    // Print args.
    std::cout << "Path:       " << args[1] << std::endl;
    std::cout << "IP:         127.0.0.1" << std::endl;
    std::cout << "PortMC:     " << args[2] << std::endl;
    std::cout << "PortBaical: " << args[3] << std::endl;
    std::cout << "Frequency:  " << args[4] << std::endl;

    // Init socket.
    int sockfd;
    const uint32_t BUFLEN = 2048;
    char buffer[BUFLEN];
    struct sockaddr_in servaddr{}, cliaddr{};

    // Creating socket file descriptor
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        die("socket creation failed");
    }

    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    uint16_t port = std::stoul(args[2], nullptr, 0);
    servaddr.sin_port = htons(port);

    // Bind the socket with the server address
    if (bind(sockfd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
        die("bind failed");
    }

    int len, n;

    len = sizeof(cliaddr);

    std::string proxy_cfg = "/P7.Name=w7500p_udp_proxy /P7.Verb=0 /P7.Addr=127.0.0.1 /P7.Port=" +
                            std::string(args[3]) + " /P7.Sink=Baical /P7.Pool=4096";
    IuP7proxy *l_iProxy = uP7createProxy(TM(proxy_cfg.c_str()), args[1]);
    if (!l_iProxy) {
        die("open proxy nullptr");
    }

    uint64_t freq = std::stoul(args[4], nullptr, 0);
    IuP7Fifo *g_pFifo = NULL;
    if (!l_iProxy->RegisterCpu(1, false, freq, TM("w7500p_udp_proxy"), 0xFFFF, true, g_pFifo)) {
        die("register cpu fail");
    }

    while (true) {
        n = recvfrom(sockfd, (char *)buffer, BUFLEN,
                     MSG_WAITALL, (struct sockaddr *)&cliaddr,
                     (socklen_t *)&len);

        if (n < 0) {
            die("error recvfrom");
        }

        if (!g_pFifo->Write(buffer, n)) {
            die("error write to baikal");
        }
    }
}
